import { Injectable } from "@angular/core";
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    Marker,
    Environment,
    HtmlInfoWindow
} from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { BehaviorSubject } from "rxjs";

import { MAP_CONFIG } from "./map.constants";
import { MapMarker, InfoWindowOption } from "./map.model";

declare var google;

@Injectable()
export class MapProvider {

    private map: GoogleMap;

    public mapAvailable: boolean = false;
    public geolocation: BehaviorSubject<Coordinates> = new BehaviorSubject(null);

    constructor(private geolocationProvider?: Geolocation) {
        this.setEnvironmentVariables(MAP_CONFIG.APP_MAP_API_KEY);
    }

    /**
     * @param mountTo -> Html element id
     * @param mapOptions -> custom GoogleMapOptions
     * @param successCallback -> called when map is ready
     * @param errorCallback -> called on error of map ready event
     */
    public createMap(mountTo: string, mapOptions?: GoogleMapOptions, successCallback?: Function, errorCallback?: Function) {
        let _mapOptions: GoogleMapOptions = mapOptions || MAP_CONFIG.APP_DEFAULT_MAP_OPTIONS;
        this.map = GoogleMaps.create(mountTo, _mapOptions);
        this.map.one(GoogleMapsEvent.MAP_READY).then(
            () => {
                this.mapAvailable = true;
                successCallback();
            },
            (err) => {
                errorCallback(err);
            }
        );
    }

    public getMap() {
        return this.map || null;
    }

    public resetMapContainer(mountTo?: string) {
        this.map.setDiv(mountTo);
    }

    /** 
     * @desc -> destroys map instance of 
     */
    public destroyMap() {
        this.map = null;
        this.mapAvailable = false;
    }

    /**
     * @desc -> initialize geolocation class member for the device.
     */
    public initGeolocation() {
        if (this.geolocationProvider) {
            this.geolocationProvider.getCurrentPosition().then(
                (res) => {
                    let coords = res.coords;
                    this.geolocation.next(coords);
                }
            ).catch((err) => {
                this.geolocation.error(err)
            })
        }
    }

    /**
     * @param latLng -> latitude and longitude of ILatLng
     * @desc -> set camera position on map
     */
    public setCameraPosition(latLng) {
        this.map.setCameraTarget(latLng);
    }

    /**
     * @param array -> List of markers of type MapMarker.
     * @param addInfoWindow -> Info window flag for marker on click.
     */
    public addMarkers(array: Array<MapMarker>, addInfoWindow?: boolean) {
        this.map.clear();
        for (let x = 0; x < array.length; x++) {
            let markerOptions: any = array[x];
            this.addMarker(markerOptions, addInfoWindow)
        }
    }

    /**
     * @desc -> supporting function for addMarkers in order to maintain each marker closure. 
     */
    private addMarker(markerOptions, addInfoWindow) {
        let marker: Marker = this.map.addMarkerSync(markerOptions);
        this.markerEventListener(marker, addInfoWindow ? markerOptions.infoWindowOptions : null);
    }

    /**
     * @param marker -> marker to which event listener will get attached
     * @param infoWindowOptions -> info window config of type InfoWindowOption for setting title, subtitle and description
     * @desc -> add marker listeners to the marker object. 
     */
    private markerEventListener(marker, infoWindowOptions?: InfoWindowOption): void {
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(
            (_marker) => {
                if (infoWindowOptions) {
                    this.openInfoWindow(marker, infoWindowOptions);
                }
            }
        )
    }

    /**
     * @param marker -> marker to which event listener will get attached
     * @param infoWindowOptions -> info window config of type InfoWindowOption for setting title, subtitle and description
     * @desc -> opens info window on marker click
     */
    private openInfoWindow(marker, infoWindowOptions?: InfoWindowOption): void {
        let htmlInfoWindow = new HtmlInfoWindow();
        let frame: HTMLElement = document.createElement('div');
        let infoWindowConfig = MAP_CONFIG.INFO_WINDOW;

        frame.innerHTML = infoWindowConfig._getTemplate(infoWindowOptions);
        htmlInfoWindow.setContent(frame, infoWindowConfig.STLYES);
        htmlInfoWindow.open(marker);
    }

    /**
     * @param apiKey -> Google map API key
     */
    private setEnvironmentVariables(apiKey): void {
        Environment.setEnv({
            API_KEY_FOR_BROWSER_RELEASE: apiKey,
            API_KEY_FOR_BROWSER_DEBUG: apiKey
        });
    }//for browser usage. development purposes
}