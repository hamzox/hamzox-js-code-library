import { MAP_CONFIG } from "./map.constants";

export class MapMarker {

    position: { lng?: number, lat?: number };
    icon?: string;
    animation?: string;
    infoWindowOptions?: InfoWindowOption;

    constructor(data?) {
        let mapConfig = MAP_CONFIG;
        if (data) {
            this.position = {
                lng: data.position.lng || mapConfig.DEFAULT_LOCATION.lng,
                lat: data.position.lat || mapConfig.DEFAULT_LOCATION.lat
            };
            this.icon = data.icon || mapConfig.MARKER.icon;
            this.animation = data.animation || mapConfig.MARKER.animation;
            this.infoWindowOptions = data.infoWindowOptions || null;
        }
    }

}

export class InfoWindowOption {

    title: string;
    subtitle?: string;
    description?: string;

    constructor(data?) {
        if (data) {
            this.title = data.title || "";
            this.subtitle = data.subtitle || "";
            this.description = data.description || "";
        }
    }

}

export class CoverageRequestModel {

    Latitude: number;
    Longitude: number;
    Radius: number;

    constructor(data?: CoverageRequestModel) {
        this.Latitude = data.Latitude || null;
        this.Longitude = data.Longitude || null;
        this.Radius = data.Radius || null;
    }

}

export class MapSearchItem {
    
    lat: number;
    lng: number;
    description: string;;
    
    constructor(lat, lng, description) {
        this.lat = lat;
        this.lng = lng;
        this.description = description;
    }

}