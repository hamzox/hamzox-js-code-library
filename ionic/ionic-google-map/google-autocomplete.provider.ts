import { GeocoderRequest, Geocoder, GeocoderResult, BaseArrayClass } from "@ionic-native/google-maps";

export class GoogleAutocompleteProvider {

    public googleAutoComplete;

    /**
     * @param google -> google global object provided by Google Maps API.
     * @desc -> Class not made injectable because parameters will be resolved by google object
     */
    constructor(google?) {
        this.googleAutoComplete = new google.maps.places.AutocompleteService(null, { types: ['geocode'] });
    }

    /**
     * @param text -> text input to search
     * @param successCallback -> success call back on completion
     * @param errorCallback -> error call back TODO
     * 
     * @desc -> get place predictions and geocode them in order to get ILatLng results
     */
    searchResults(text: string, successCallback?: Function, errorCallback?: Function) {
        let autoCompleteItems = [];
        if (text === '' || text.length === 0) {
            successCallback(autoCompleteItems);
            return;
        }
        this.googleAutoComplete.getPlacePredictions({ input: text },
            (predictions, status) => {
                autoCompleteItems = [];
                if (!predictions || predictions.length === 0) {
                    successCallback(autoCompleteItems);
                    return;
                }

                //we have autocomplete predictions in 'predictions' variable but with no longitudes and latitudes.
                let options: GeocoderRequest = {
                    address: predictions.map((prediction) => prediction.description)
                };
                Geocoder.geocode(options).then((mvcArray: BaseArrayClass<GeocoderResult[]>) => {
                    mvcArray.one('finish').then(() => {
                        autoCompleteItems = mvcArray.getArray().filter((res) => res);
                        successCallback(autoCompleteItems);
                    });
                });
            });
    }
}