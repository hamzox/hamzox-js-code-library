import { WindowOptiInfoWindowOptionon } from "./map.model";

const defaultLocation = {
    lat: 37.9643,
    lng: -91.8318
} //default location for the map (Missouri) if location is not enabled

export const MAP_CONFIG = {
    APP_DEFAULT_MAP_OPTIONS: {
        camera: {
            target: {
                lat: defaultLocation.lat,
                lng: defaultLocation.lng
            },
            zoom: 10,
            tilt: 30
        }
    },
    APP_MAP_API_KEY: 'APP_API_KEY',
    DEFAULT_LOCATION: defaultLocation,
    MARKER: {
        icon: '#C42031',
        animation: 'DROP',
    },
    INFO_WINDOW: {
        STLYES: {
            width: "280px",
            height: "100px"
        },
        _getTemplate(infoWindowOption: InfoWindowOption): string {
            return [`<h2>${infoWindowOption.title}</h2>`, `<h3>${infoWindowOption.subtitle}</h3>`, `<p>${infoWindowOption.description}<p>`].join("");
        } //info window HTML template
    }
}