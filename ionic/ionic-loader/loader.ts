import { Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';

/*
 * @Author: hamzox 
 * @Date: 2018-10-10 10:53:14 
 * @Last Modified by: Muhammad Umair
 * @Last Modified time: 2018-11-26 13:24:10
 * 
 * @Class:  LoaderProvider
 * @Description: Create/dismiss loader.
 */

@Injectable()
export class LoaderProvider {

    private loader: Loading;
    public isLoaderActive: boolean = false;

    constructor(
        private loaderCtrl: LoadingController
    ) { }

    private createLoader() {
        this.loader = this.loaderCtrl.create();
    }

    public showLoader(): Promise<any> {
        this.createLoader();
        this.isLoaderActive = true;
        return this.loader.present();
    }

    public hideLoader() {
        this.isLoaderActive = false;
        this.loader.dismiss();
    }

}