import { Camera, CameraOptions } from "@ionic-native/camera";
import { Injectable } from "@angular/core";
import { LoaderProvider } from "../loader/loader";
import { DomSanitizer } from "@angular/platform-browser";

declare var cordova;

@Injectable()
export class ImageUploadProvider {

    constructor(
        private camera: Camera,
        private loader: LoaderProvider,
        private sanitizer: DomSanitizer
    ) { }

    public lastImage: string = null;

    public takePicture(sourceType, successCallback?: Function, errorCallback?: Function) {
        // Create options for the Camera Dialog
        var options: CameraOptions = {
            quality: 10,
            sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.loader.showLoader().then(
            () => {
                this.camera.getPicture(options).then((imagePath) => {
                    let base64Img = `data:image/jpeg;base64,${imagePath}`;
                    this.loader.hideLoader();
                    successCallback(base64Img);
                }, (err) => {
                    this.loader.hideLoader();
                    errorCallback(err);
                });
            }
        ).catch(() => this.loader.hideLoader());

    }
}