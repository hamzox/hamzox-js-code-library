import { Injectable } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

/* 
 * @Class: FormService
 * @Description: Provide utility form functions for angular.
 */

@Injectable()
export class FormProvider {

    constructor(private formBuilder: FormBuilder) {
        console.log("Form service loaded...");
    }

    /**
     * @param form -> FormGroup
     * @param keys -> Array of objects, { key: string, required?: boolean }
     * @param values -> Array of values. (Must be of same length as of keys)
     */

    addFormControls(form: FormGroup, keys: Array<any>, values?, validators?) {
        keys.forEach((el, index) => form.addControl(el.key, new FormControl(values ? values[index] || null : null, el.required ? Validators.required : null)))
    }

    createForm(form: FormGroup, formControls?: Array<any>, values?, validators?) {
        form = this.formBuilder.group({}); //initialize instance of form group.
        if (formControls) {
            this.addFormControls(form, formControls, values, validators);
        }
        return form;
    } //creates and returns form group.

    setValidators(form, formControlName: string, validators: Array<any>) {
        form.controls[formControlName].setValidators(Validators.compose(validators));
        return form;
    } //set validators to the form group given form control name and validators array.

    isFieldEqualTo(field_name): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let input = control.value;
            let isValid = control.root.value[field_name] == input;
            if (!isValid)
                return { 'equalTo': { isValid } };
            else
                return null;
        };
    }
}