import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslationProvider } from '../../providers/translation/translation.provider';
import { Translation } from '../../providers/translation/translation.interface';

@Component({
    selector: 'form-error',
    templateUrl: 'form-error.component.html'
})

export class FormErrorComponent {

    @Input() fValidationMessages: Array<any>;
    @Input() fCtrlName: string; //form control name as string
    @Input() fName: FormGroup; //form group object

    translations: Translation;

    constructor(
        private tranlator: TranslationProvider
    ) { }

    ngOnInit(): void {
        this.translations = this.tranlator.translations;
    }

}