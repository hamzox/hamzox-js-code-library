import React from 'react';
import 'react-circular-progressbar/dist/styles.css';
import CircularProgressbar from 'react-circular-progressbar';

/**
 * REFERNCE: https://www.npmjs.com/package/react-circular-progressbar
 */

//BMProgressBar Default Config
const DEFAULT_CONFIG = {
    // size: 150,
    text: 0,
    percentage: 0,
    val: 0,
    style: {
        path: { stroke: `#8a1503` },
        text: { fill: '#8a1503', fontSize: '16px' },
    }
}

class BMProgressBar extends React.Component {

    render() {
        const { style, percentage, val, size } = this.props.config || {};
        return (
            <div className="chart" style={{ width: size || DEFAULT_CONFIG.size }}>
                <CircularProgressbar
                    percentage={percentage || DEFAULT_CONFIG.percentage}
                    text={`${val || DEFAULT_CONFIG.val}%`}
                    styles={style || DEFAULT_CONFIG.style}
                />
            </div>
        );
    }

}

export default BMProgressBar;